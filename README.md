# Project name and key lock plugin

When installed and enabled, prevents the project key or name from being edited by a user who is not a sysadmin.

Project Admins will still be able to change the description and avatar.

## Requirements

* Stash 2.3.0 or later

## Screenshot

![screenshot](http://i39.tinypic.com/2vuykcg.jpg)

## Limitations and to do

This is a global plugin - and currently does not support a configuration option to turn this on/off per project.

# Enhance this plugin

Fork it, send back a pull request!

## Running the plugin

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK


## Docs

Full documentation is always available at the [Stash Developer Docs](developer.atlassian.com/stash/docs/latest/index.html).
