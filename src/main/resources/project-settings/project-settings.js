(function ($) {
    $(document).ready(function () {
        var pageState = require('model/page-state');
        var isAdmin = pageState.getCurrentUser().isAdmin;
        var inputFields = $("input[name=key],input[name=name]");

        if (pageState && !isAdmin) {
            inputFields
                .attr("readonly", "readonly")
                .addClass("disabled")
                .siblings(".description")
                    .text("Contact your Stash administrator to edit this project's details.");
        }
    });
})(AJS.$);
