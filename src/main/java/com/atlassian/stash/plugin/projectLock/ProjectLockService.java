package com.atlassian.stash.plugin.projectLock;

import com.atlassian.stash.project.Project;

public interface ProjectLockService {

    boolean isProjectEditingLocked(Project project);

    void setProjectEditingLocked(Project project);
}
