package com.atlassian.stash.plugin.projectLock;


import com.atlassian.event.api.EventListener;
import com.atlassian.stash.event.ProjectModificationRequestedEvent;
import com.atlassian.stash.i18n.KeyedMessage;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionService;

public class ProjectModificationRequestedEventListener {
    private final ProjectLockService projectLockService;
    private final PermissionService permissionService;
    private ProjectModificationRequestedEvent event;

    public ProjectModificationRequestedEventListener(ProjectLockService projectLockService, PermissionService permissionService) {
        this.projectLockService = projectLockService;
        this.permissionService = permissionService;
    }

    @EventListener
    public void onProjectModificationRequestedEvent(ProjectModificationRequestedEvent event) {
        this.event = event;
        handleProjectNameChange();
        handleProjectKeyChange();
    }

    private boolean isSysAdmin() {
        return permissionService.hasGlobalPermission(event.getUser(), Permission.ADMIN);
    }

    private boolean hasChangePermission() {
        boolean isProjectEditingLocked = true; //projectLockService.isProjectEditingLocked(event.getProject());
        return isSysAdmin() && isProjectEditingLocked;
    }

    private void handleProjectKeyChange() {
        String oldName = event.getOldValue().getKey();
        String newName = event.getNewValue().getKey();
        boolean projectNameChanged = !oldName.equals(newName);

        if (projectNameChanged && !hasChangePermission()) {
            cancelChangeWithMessage("The project key can only be changed by a System Administrator.");
        }
    }

    private void handleProjectNameChange() {
        String oldName = event.getOldValue().getName();
        String newName = event.getNewValue().getName();
        boolean projectNameChanged = !oldName.equals(newName);

        if (projectNameChanged && !hasChangePermission()) {
            cancelChangeWithMessage("The project name can only be changed by a System Administrator.");
        }
    }

    private void cancelChangeWithMessage(String error) {
        KeyedMessage message = new KeyedMessage("stash.plugin.projectLock.locked", error, error);
        event.cancel(message);
    }
}
