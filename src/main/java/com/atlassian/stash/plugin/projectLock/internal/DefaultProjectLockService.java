package com.atlassian.stash.plugin.projectLock.internal;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.plugin.projectLock.ProjectLockService;
import com.atlassian.stash.project.Project;

public class DefaultProjectLockService implements ProjectLockService {

    private final PluginSettings pluginSettings;

    public DefaultProjectLockService(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createSettingsForKey("com.atlassian.stash.plugin.projectLock");
    }

    @Override
    public boolean isProjectEditingLocked(Project project) {
        String flag = (String) pluginSettings.get(project.getKey());
        return "true".equals(flag);
    }

    @Override
    public void setProjectEditingLocked(Project project) {
        pluginSettings.put(project.getKey(), "true");
    }
}
